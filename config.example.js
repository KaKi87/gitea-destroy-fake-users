/** @type {string} */
export const webRoot = '';

/** @type {string} */
export const accessToken = '';

/** @type {string[]} */
export const whitelist = [];