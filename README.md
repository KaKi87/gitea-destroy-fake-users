# deno-gitea-destroy-fake-users

Automatically destroy fake Gitea users (e.g. have never logged in but have repos, or have no activity record whatsoever).

Usage : `cp config.example.js config.js` ; `deno run --allow-net main.js`

## Warning

This tool may detect inactive but legitimate users.