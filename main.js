import axios from 'npm:axios';
import AsciiTable from 'npm:ascii-table';
import {
    webRoot,
    accessToken,
    whitelist
} from './config.js';

const
    client = axios.create({
        baseURL: `${webRoot}/api/v1`,
        params: {
           'access_token': accessToken 
        }
    }),
    getRepos = async username => (await client(`/users/${username}/repos`)).data.map(({ name }) => name),
    getOrgs = async username => (await client(`/users/${username}/orgs`)).data.map(({ username }) => username),
    getUser = async (username, user) => {
        const
            {
                'last_login': lastLoginDate,
                'starred_repos_count': starCount,
                'following_count': followCount,
                'description': description,
                'website': websiteUrl
            } = user || (await client(`/users/${username}`)).data,
            isActive = lastLoginDate !== '1970-01-01T01:00:00+01:00',
            hasStars = starCount > 0,
            hasFollows = followCount > 0,
            repos = await getRepos(username),
            hasRepos = repos.length > 0,
            orgs = await getOrgs(username),
            hasOrgs = orgs.length > 0,
            hasHistory = !!(await client(`/users/${username}/activities/feeds`)).data.length,
            hasDescription = !!description,
            hasWebsite = !!websiteUrl;
        return {
            isActive,
            hasStars,
            hasFollows,
            repos,
            hasRepos,
            orgs,
            hasOrgs,
            hasHistory,
            hasDescription,
            hasWebsite
        };
    },
    deleteRepo = (username, repo) => client.delete(`/repos/${username}/${repo}`),
    table = new AsciiTable().setHeading('Username', 'Has repos/orgs', 'Has stars/follows', 'Has history', 'Has description', 'Has website', 'URL'),
    data = {};
if(Deno.args[0]){
    const
        [username] = Deno.args,
        {
            hasStars,
            hasFollows,
            repos,
            hasRepos,
            orgs,
            hasOrgs,
            hasHistory,
            hasDescription,
            hasWebsite
        } = await getUser(username);
    data[username] = { repos, orgs };
    table.addRow(username, hasRepos || hasOrgs, hasStars || hasFollows, hasHistory, hasDescription, hasWebsite, `${webRoot}/${username}`);
}
else {
    for(const user of (await client('/admin/users')).data){
        const
            username = user['login'],
            {
                isActive,
                hasStars,
                hasFollows,
                repos,
                hasRepos,
                orgs,
                hasOrgs,
                hasHistory,
                hasDescription,
                hasWebsite
            } = await getUser(username);
        if(
            !whitelist.includes(username)
            &&
            ((hasRepos || hasOrgs)
                ? !isActive
                : (!hasStars && !hasFollows && !hasHistory && (hasDescription || hasWebsite)))
        ){
            data[username] = { repos, orgs };
            table.addRow(username, hasRepos || hasOrgs, hasStars || hasFollows, hasHistory, hasDescription, hasWebsite, `${webRoot}/${username}`);
        }
    }
}
if(Object.keys(data).length){
    console.log(table.toString());
    if(prompt('Confirm ? (y/N) >') === 'y'){
        for(const [username, { repos, orgs }] of Object.entries(data)){
            for(const repo of repos){
                console.log('Deleting repo:', repo);
                await deleteRepo(username, repo);
            }
            for(const org of orgs){
                const orgRepos = await getRepos(org);
                for(const repo of orgRepos){
                    console.log('Deleting repo:', repo);
                    await deleteRepo(org, repo);
                }
                console.log('Deleting org:', org);
                await client.delete(`/orgs/${org}`);
            }
            console.log('Deleting user:', username);
            await client.delete(`/admin/users/${username}`, { params: { 'purge': 'true' } });
        }
        console.log('Done');
    }
    else
        console.log('Aborted');
}
else
    console.log('None found');